import jwt from 'jsonwebtoken';
import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import petRepository from '../../repositories/petsRepository';
import lockRepository from '../../repositories/lockRepository';
import { env } from '../../config/dataConfig';
import { IListFilter } from '../interfaces/interfaces';
import sendSMS from '../helpers/smsHelper';

export const getUsers = async (filter: IListFilter) => await userRepository.getList(filter);
export const getUserById = async (userId: string) => await userRepository.getUserById(userId);

export const lock = async (id: string, data: any) => {
  const { userId, lockId } = data;
  const idd = userId || id;
  return await userRepository.lock(idd, lockId);
}

export const unlock = async (id: string, data: any) => {
  const { userId, lockId } = data;
  const idd = userId || id;
  return await userRepository.unlock(idd, lockId);
}

export const register = async (newUser: any) => {
  const user = await userRepository.create(newUser);
  const { id } = user;
  const token = user ?  jwt.sign({ id }, env.crypto_key) : null;
  return { token, user }
}

export const login = async ( req: Request, res: Response ) => {
  const { phone, code, repeat } = req.body;
  const user = await userRepository.getByPhone(phone);
  if (user) {
    if (code) {
      if (user.verify === code) {
        const { id } = user;
        const token = user ?  jwt.sign({ id }, env.crypto_key) : null;
        await userRepository.updateById(user.id, { verify: '' });
        return res.status(200).send({ token, user });
      }
      return res.status(403).send({ status: 403, message: 'Incorrect code. Try again ...' });
    }
    // eslint-disable-next-line no-shadow
    // const vCode = repeat ? user.verify : Math.round(1000 + 9000 * Math.random()).toString();
    const vCode = '1234';
    await userRepository.updateById(user.id, { verify: vCode });
    // const result: any = await sendSMS(phone, vCode);
    const result: any = "SM5b3d977b595d4af9bbf8c10dcde7eb92";
    if (result.error) {
      return res.status(403).send({ status: 403, message: result.error });
    } else {
      return res.status(200).send({ status: 200, result });
    }
  }
  return res.status(404).send({ status: 404, message: 'No such phone ...' });
}

export const updateUser = async (userId: string, updUser: any) => {
  const id = updUser.id || userId;
  return await userRepository.updateById(id, updUser);
}

export const deleteUser = async (id: string) =>  {
  const { result: pets } = await petRepository.deleteUserPets(id);
  const { result: locks } = await lockRepository.deleteLocks(id);
  const { result } = await userRepository.delete(id);
  return { result, pets, locks };
}

export interface IPwd {
  pwd: string
}

export const doAdminAuth = async (PWD: IPwd = { pwd: '' }) => {
  const { pwd } = PWD;
  const { adminName, adminPWD } = env.admin;
  if (pwd === adminPWD) {
    let admin = await userRepository.getByName(adminName);
    if (!admin) {
      admin = await userRepository.create({ username: adminName, phone: 'no-admin-phone' });
    }
    const { id } = admin;
    const token = jwt.sign({ id }, env.crypto_key);
    return { token, user: admin };
  }
  return { status: 403, message: 'Incorrect admin password given. Try again ...' };
}
