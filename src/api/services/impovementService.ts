import impoveRepository from '../../repositories/impovementRepository';
import { IMessage, IRequest } from '../interfaces/interfaces';

export const create = async (req: IRequest) => {
  const userId = req.user;
  const body = req.body;
  const result = await impoveRepository.create({ userId, ...body });
  return result;
};
export const update = async (item: IMessage) => await impoveRepository.updateById(item.id, item);
export const getById = async (id: string) => await impoveRepository.getById(id);
export const getImpove = async (filter: object) => await impoveRepository.getImpove(filter);
export const getImpoveList = async (filter: object) => await impoveRepository.getList(filter);
export const deleteUserImpove = async (userId: string) => await impoveRepository.deleteUserImpove(userId);
export const deleteImpove = async (id: string) => await impoveRepository.delete(id);
