import strollRepository from '../../repositories/strollsRepository';
import { IListFilter } from '../interfaces/interfaces';

export const getStrolls = async (filter: IListFilter) => await strollRepository.getList(filter);
export const getStrollById = async (id: string) => await strollRepository.getById(id);
export const getStrollByPet = async (petId: string) => await strollRepository.getByPet(petId);
export const createStroll = async (data: any) => await strollRepository.createStroll(data);
export const joinStroll = async (data: any) => await strollRepository.joinStroll(data);
export const leaveStroll = async (data: any) => await strollRepository.leaveStroll(data);
export const updateStroll = async (id: string, data: any) => await strollRepository.updateById(id, data);
export const deleteStroll = async (id: string) => await strollRepository.delete(id);