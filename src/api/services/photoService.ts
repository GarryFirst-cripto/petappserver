import { Request } from 'express';
import userRepository from '../../repositories/usersRepository';
import petRepository from '../../repositories/petsRepository';
import { uploadFile, deleteFile } from '../helpers/awsHelper';

export const createPhoto = async (req: Request) => {
  const { params: { id } } = req;
  const { file: { buffer } } = req;
  const user = await userRepository.getById(id);
  if (user) {
    await deleteFile(user.photo);
    const link = await uploadFile(buffer);
    return await userRepository.updateById(id, { photo: link });
  }
  const pet = await petRepository.getById(id);
  if (pet) {
    await deleteFile(pet.photo);
    const link = await uploadFile(buffer);
    return await petRepository.updateById(id, { photo: link });
  }
  return { error: 'No such user or such pet ...' };
}
