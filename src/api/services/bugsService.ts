import bugsRepository from '../../repositories/bugsRepository';
import { IMessage, IRequest } from '../interfaces/interfaces';

export const create = async (req: IRequest) => {
  const userId = req.user;
  const body = req.body;
  const result = await bugsRepository.create({ userId, ...body });
  return result;
};
export const update = async (item: IMessage) => await bugsRepository.updateById(item.id, item);
export const getById = async (id: string) => await bugsRepository.getById(id);
export const getBugs = async (filter: object) => await bugsRepository.getBugs(filter);
export const getBugsList = async (filter: object) => await bugsRepository.getBugsList(filter);
export const deleteUserBugs = async (userId: string) => await bugsRepository.deleteUserBugs(userId);
export const deleteBug = async (id: string) => await bugsRepository.delete(id);
