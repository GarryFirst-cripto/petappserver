import petRepository from '../../repositories/petsRepository';
import petFriendRepository from '../../repositories/petFriendRepository';
import { IListFilter } from '../interfaces/interfaces';

export const getPets = async (filter: IListFilter) => await petRepository.getList(filter);
export const getPetById = async (userId: string) => await petRepository.getById(userId);
export const createPet = async (id: string, data: any) => {
  const userId = data.userId || id;
  data.userId = userId;
  return await petRepository.create(data);
}
export const addPetFriend = async (data: any) => await petRepository.addPetFriend(data);
export const appruveFriend = async (data: any) => await petFriendRepository.appruveFriend(data);
export const removePetFriend = async (data: any) => await petRepository.removePetFriend(data);
export const updatePet = async (id: string, data: any) => await petRepository.updateById(id, data);
export const deletePet = async (id: string) => await petRepository.delete(id);