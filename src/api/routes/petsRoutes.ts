import { Router } from 'express';
import * as petsService from '../services/petsService';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import petMiddleware from '../middlewares/petMiddleware';

const router = Router();

router
  .get('/:id', (req, res, next) => petsService.getPetById(req.params.id)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .get('/', (req, res, next) => petsService.getPets(<IListFilter> <unknown>req.query)
    .then((pets: any) => res.send(pets))
    .catch(next))
  .post('/', petMiddleware, (req, res, next) => petsService.createPet((<IRequest> req).user.id, req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .post('/addfriend', (req, res, next) => petsService.addPetFriend(req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .post('/appruve', (req, res, next) => petsService.appruveFriend(req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .post('/removefriend', (req, res, next) => petsService.removePetFriend(req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .put('/:id', (req, res, next) => petsService.updatePet(req.params.id, req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .delete('/:id', (req, res, next) => petsService.deletePet(req.params.id)
    .then((delPet: any) => res.send(delPet))
    .catch(next));

export default router;