import { Router } from 'express-serve-static-core';
import usersRoutes from './usersRoutes';
import petsRoutes from './petsRoutes';
import photoRoutes from './photoRoutes';
import strollRoutes from './strollRoutes';
import impovementRoutes from './impovementRoutes';
import bugsRoutes from './bugsRoutes';

export default (app: { use: (arg0: string, arg1: Router) => void; }) => {
  app.use('/api/users', usersRoutes);
  app.use('/api/pets', petsRoutes);
  app.use('/api/photo', photoRoutes);
  app.use('/api/strolls', strollRoutes);
  app.use('/api/impovement', impovementRoutes);
  app.use('/api/bugs', bugsRoutes);
};
