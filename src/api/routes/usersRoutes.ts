import { Router } from 'express';
import * as userService from '../services/usersServices';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import registrMiddleware from '../middlewares/registrMiddleware';
import updateMiddleware from '../middlewares/updateMiddleware';

const router = Router();

router
  .get('/user', (req, res, next) => userService.getUserById((<IRequest> req).user.id)
    .then(user => res.send(user))
    .catch(next))
  .get('/admin/auth', (req, res, next) => userService.doAdminAuth(<userService.IPwd> <unknown>req.query)
    .then(data => res.send(data))
    .catch(next))
  .get('/:id', (req, res, next) => userService.getUserById(req.params.id)
    .then(users => res.send(users))
    .catch(next))
  .get('/', (req, res, next) => userService.getUsers(<IListFilter> <unknown>req.query)
    .then(users => res.send(users))
    .catch(next))
  .post('/register', registrMiddleware, (req, res, next) => userService.register(req.body)
    .then(data => res.send(data))
    .catch(next))
  .post('/login', (req, res, next) => userService.login(req, res)
    .catch(next))
  .post('/lock', (req, res, next) => userService.lock((<IRequest> req).user.id, req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .post('/unlock', (req, res, next) => userService.unlock((<IRequest> req).user.id, req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .put('/', updateMiddleware, (req, res, next) => userService.updateUser((<IRequest> req).user.id, req.body)
    .then(user => res.send(user))
    .catch(next))
  .delete('/:id', (req, res, next) => userService.deleteUser(req.params.id)
    .then(delUser => res.send(delUser))
    .catch(next));

export default router;
