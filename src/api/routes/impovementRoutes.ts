import { Router } from 'express';
import { IRequest } from '../interfaces/interfaces';
import * as impoveService from '../services/impovementService';

const router = Router();

router
  .get('/list', (req, res, next) => impoveService.getImpoveList(req.query)
    .then(items => res.send(items))
    .catch(next))
  .get('/:id', (req, res, next) => impoveService.getById(req.params.id)
    .then(items => res.send(items))
    .catch(next))
  .get('/', (req, res, next) => impoveService.getImpove(req.query)
    .then(items => res.send(items))
    .catch(next))
  .post('/', (req, res, next) => impoveService.create(<IRequest>req)
    .then(item => res.send(item))
    .catch(next))
  .put('/', (req, res, next) => impoveService.update(req.body)
    .then((item) => res.send(item))
    .catch(next))
  .delete('/user/:userId', (req, res, next) => impoveService.deleteUserImpove(req.params.userId)
    .then(item => res.send(item))
    .catch(next))
  .delete('/:id', (req, res, next) => impoveService.deleteImpove(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;