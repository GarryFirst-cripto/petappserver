import { Router } from 'express';
import * as strollService from '../services/strollsService';
import { IRequest, IListFilter } from '../interfaces/interfaces';
import strollMiddleware from '../middlewares/strollMiddleware';

const router = Router();

router
  .get('/petstroll/:id', (req, res, next) => strollService.getStrollByPet(req.params.id)
    .then((stroll: any) => res.send(stroll))
    .catch(next))
  .get('/:id', (req, res, next) => strollService.getStrollById(req.params.id)
    .then((stroll: any) => res.send(stroll))
    .catch(next))
  .get('/', (req, res, next) => strollService.getStrolls(<IListFilter> <unknown>req.query)
    .then((strolls: any) => res.send(strolls))
    .catch(next))
  .post('/join', (req, res, next) => strollService.joinStroll(req.body)
    .then((stroll: any) => res.send(stroll))
    .catch(next))
  .post('/leave', (req, res, next) => strollService.leaveStroll(req.body)
    .then((stroll: any) => res.send(stroll))
    .catch(next))    
  .post('/', strollMiddleware, (req, res, next) => strollService.createStroll(req.body)
    .then((stroll: any) => res.send(stroll))
    .catch(next))
  .put('/:id', (req, res, next) => strollService.updateStroll(req.params.id, req.body)
    .then((pet: any) => res.send(pet))
    .catch(next))
  .delete('/:id', (req, res, next) => strollService.deleteStroll(req.params.id)
    .then((delStroll: any) => res.send(delStroll))
    .catch(next));

export default router;