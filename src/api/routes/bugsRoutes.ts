import { Router } from 'express';
import { IRequest } from '../interfaces/interfaces';
import * as bugsService from '../services/bugsService';

const router = Router();

router
  .get('/list', (req, res, next) => bugsService.getBugsList(req.query)
    .then(items => res.send(items))
    .catch(next))
  .get('/:id', (req, res, next) => bugsService.getById(req.params.id)
    .then(items => res.send(items))
    .catch(next))
  .get('/', (req, res, next) => bugsService.getBugs(req.query)
    .then(items => res.send(items))
    .catch(next))
  .post('/', (req, res, next) => bugsService.create(<IRequest>req)
    .then(item => res.send(item))
    .catch(next))
  .put('/', (req, res, next) => bugsService.update(req.body)
    .then((item) => res.send(item))
    .catch(next))
  .delete('/user/:userId', (req, res, next) => bugsService.deleteUserBugs(req.params.userId)
    .then(item => res.send(item))
    .catch(next))
  .delete('/:id', (req, res, next) => bugsService.deleteBug(req.params.id)
    .then(item => res.send(item))
    .catch(next));

export default router;