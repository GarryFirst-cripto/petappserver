import { Router } from 'express';
import * as photoService from '../services/photoService';
import fileMiddleware from '../middlewares/fileMiddleware';

const router = Router();

router
  .post('/:id', fileMiddleware, (req, res, next) => photoService.createPhoto(req)
    .then((result: any) => res.send(result))
    .catch(next));

export default router;