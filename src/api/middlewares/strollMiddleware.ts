import { Request, Response } from 'express';
import petRepository from '../../repositories/petsRepository';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { petId } = req.body;
  if (petId) {
    const petById = await petRepository.getById(petId);
    if (!petById) {
      return res.status(404).send({ status: 404, message: 'No pet with such ID.' });
    }
    return next();
  }
  return res.status(404).send({ status: 404, message: 'No pet ID given' });
};