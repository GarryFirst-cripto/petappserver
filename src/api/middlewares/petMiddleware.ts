import { Request, Response } from 'express';
import userRepository from '../../repositories/usersRepository';
import { IRequest } from '../interfaces/interfaces';

export default async (req: Request, res: Response, next: () => void): Promise<any> => {
  const { id } = (<IRequest> req).user;
  const userId = id || req.body.userId;
  const userById = await userRepository.getById(userId);
  if (!userById) {
    return res.status(404).send({ status: 404, message: 'No user with such ID.' });
  }
  return next();
};