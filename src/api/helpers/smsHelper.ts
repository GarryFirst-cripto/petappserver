import Client from 'twilio';
// import { SMSRu } from 'node-sms-ru';
import { env } from '../../config/dataConfig';

const client = Client(env.smss.accountSid, env.smss.authToken);

const sendSMS = async (phone: string, code: string) => {
  try {
    const send = new Promise((resolve, reject) => {
      client.messages.create({ body: `Your confirm code : ${code}`, from: '+15033609376', to: phone })
      .then((message: any) => { resolve(message.sid)})
      .catch((err: any) => reject(err))
    })
    return await send;
  } catch (e) {
    return { error: 'Twilio auth error ...'}
  }
}

export default sendSMS;

// const key = env.sms.apiKey;
// const smsRu = new SMSRu(key);
// const lp = String.fromCharCode(13, 10);

// const sendSms = async (phone: string, code: string) => {
//   const message = `Доброго времени суток ! Ваш контрольный код : ${code} .${lp} С наилучшими пожеланиями !`;
//   const result = await smsRu.sendSms({
//     from: 'Store App Server',
//     to: phone,
//     msg: message
//   });
//   const { status, sms } = result;
//   return { status, sms };
// };

// export default async (phone: string, code: string) => {
//   try {
//     const result = await sendSms(phone, code);
//     // const result = { phone, kode };
//     return result;
//   } catch (err) {
//     return err;
//   }
// };
