import AWS from 'aws-sdk';
import genUniqueId from './uniqueHelper';
import { env } from '../../config/dataConfig';

AWS.config.update({
  region: 'us-west-1',
  accessKeyId: env.aws.accessKeyId,
  secretAccessKey: env.aws.secretAccessKey
});

const { bucketName } = env.aws;
const testAwsBucket = async () => {
  const list = await new AWS.S3().listBuckets().promise();
  if (list.Buckets.some(item => item.Name === bucketName)) return;
  await new AWS.S3({ apiVersion: '2006-03-01' }).createBucket({ Bucket: bucketName }).promise();
  console.log(`Amazon bucket "${bucketName}" created.`);
}
testAwsBucket();

export async function uploadFile(buffer: any) {
  const storeName = genUniqueId({ length: 12 }) + '.jpg';
  const objectParams = { ACL: "public-read", Bucket: bucketName, Key: storeName, Body: buffer };
  await new AWS.S3().putObject(objectParams).promise();
  const link = new AWS.S3().getSignedUrl('getObject', { Bucket: bucketName, Key: storeName });
  return link.substr(0, link.indexOf('?'));
}

export const deleteFile = async (fileName: string) => {
  if (fileName) {
    const keyName = fileName.substr(fileName.lastIndexOf('/') + 1);
    const objectParams = { Bucket: bucketName, Key: keyName };
    const result = await new AWS.S3().deleteObject(objectParams).promise();
    return result;
  }
  return null;
}
