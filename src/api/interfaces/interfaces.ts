import { Request } from 'express';

export interface IRequest extends Request {
  user: { 
    id: string;
    sex: string;
  }
}

export interface IToken {
  id: string;
}

export interface IListFilter {
  from: number;
  count: number;
  required?: boolean;
  translations?: number;
  group: string;
  status?: string;
  answer?: string;
}

export interface IUser {
  id: string;
  username: string;
  sex: string;
  age: Date;
  pushId: string;
  status: string;
  sensay: string;
  verify: string;
}

export interface ILinks {
  fileId?: string;
  link?: string;
}

export interface IMessage {
  id: string;
  userId: string;
  text: string;
  status: string;
}
