import { BugModel, UserModel } from './models/index';
import BaseRepository from './baseRepository';

class StatRepository extends BaseRepository {

  async getBugsList(filter: object) {
    return await this.model.findAll({
      where: { ...filter },
      include: {
        model: UserModel
      }
    });
  }

  async getBugs(filter: object) {
    return await this.model.findAll({ where: filter });
  }

  async getById(id: string) {
    return await this.model.findOne({
      where: { id },
      include: {
        model: UserModel
      }
    });
  }

  async deleteUserBugs(userId: string) {
    const result = await this.model.destroy({ where: { userId } });
    return { result };
  }

}

export default new StatRepository(BugModel);