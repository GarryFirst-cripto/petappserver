import { StrollToPetModel } from './models/index';
import BaseRepository from './baseRepository';

class StrollMemberRepository extends BaseRepository {
  model: any;

  async getStrollMember(strollId:string, petId:string) {
    return await this.model.findOne({ where: {strollId, petId} })
  }

  async deleteStrollMember(strollId:string, petId:string) {
    return await this.model.destroy({ where: {strollId, petId} })
  }

  async deleteStroll(strollId:string) {
    return await this.model.destroy({ where: { strollId } })
  }

  async deletePetStroll(petId:string) {
    return await this.model.destroy({ where: { petId } })
  }

}

export default new StrollMemberRepository(StrollToPetModel);