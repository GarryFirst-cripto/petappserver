import { StrollModel, PetModel, UserModel } from './models/index';
import BaseRepository from './baseRepository';
import StrollMemberRepository from './strollMemberRepository';
import { IListFilter } from '../api/interfaces/interfaces';

class StrollRepository extends BaseRepository {
  model: any;

  async createStroll(data: any) {
    const stroll = await this.create(data);
    if (stroll) {
      const { id: strollId } = stroll;
      const { petId } = data;
      await StrollMemberRepository.create({ strollId, petId });
      return this.getById(strollId);
    }
    return {}
  }

  async joinStroll(data: any) {
    const { strollId, petId } = data;
    const member = await StrollMemberRepository.getStrollMember(strollId, petId);
    if (!member) {
      await StrollMemberRepository.create({ strollId, petId });
    }
    return this.getById(strollId);
  }

  async leaveStroll(data: any) {
    const { strollId, petId } = data;
    const result = await StrollMemberRepository.deleteStrollMember(strollId, petId);
    return { result };
  }

  async getById(id: string) {
    return this.model.findOne({
      where: { id },
      include: {
        model: PetModel,
        include: {
          model: UserModel
        }
      } 
    });
  }

  async getByPet(petId: string) {
    return PetModel.findOne({
      where: { id: petId },
      include: {
        model: StrollModel,
        include: {
          model: PetModel,
          include: {
            model: UserModel
          }
        } 
      } 
    });
  }

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      include: {
        model: PetModel,
        include: {
          model: UserModel
        }
      },
      offset,
      limit
    });
  }
 
  // async deleteUserStrolls(userId: string) {
  //   const result = await this.model.destroy({ where: { userId } });
  //   return { result };
  // }

  async delete(id: string) {
    const strolls = await StrollMemberRepository.deleteStroll(id);
    const result = await this.model.destroy({ where: { id } });
    return { result, strolls };
  }

}

export default new StrollRepository(StrollModel);
