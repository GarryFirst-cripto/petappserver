import { UserModel, PetModel, LockedModel, LockUserModel } from './models/index';
import BaseRepository from './baseRepository';
import LockRepository from './lockRepository';
import { deleteFile } from '../api/helpers/awsHelper';
import { IListFilter } from '../api/interfaces/interfaces';

class UserRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where, 
      order:['username'],
      include: [{
        model: PetModel
      },{
        model: LockUserModel,
        include: {
          model: LockedModel
        }
      },{
        model: LockUserModel,
        as: 'blocked',
        include: {
          model: LockedModel,
          as: 'blockuser'
        }
      }],
      offset, 
      limit
    });
  }

  async getUserById(id: string) {
    return await this.model.findOne({
      where: { id },
      include: [{
        model: PetModel
      },{
        model: LockUserModel,
        include: {
          model: LockedModel
        }
      },{
        model: LockUserModel,
        as: 'blocked',
        include: {
          model: LockedModel,
          as: 'blockuser'
        }
      }]
    });
  }

  async getByPhone(phone: string) {
    return await this.model.findOne({ where: { phone } });
  }

  async getByName(username: string) {
    return await this.model.findOne({ where: { username } });
  }
  
  async delete(id: string) {
    const user = await this.getById(id);
    if (user) {
      await deleteFile(user.photo);
    }
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

  async lock(userId: string, lockId: string) {
    const lock = await LockRepository.getData(userId, lockId);
    if (!lock) {
      await LockRepository.create({ userId, lockId});
    }
    return this.getUserById(userId);
  }

  async unlock(userId: string, lockId: string) {
    const result = await LockRepository.deleteData(userId, lockId);
    return { result };
  }

}

export default new UserRepository(UserModel);
