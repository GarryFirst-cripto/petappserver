import { DataTypes } from "sequelize";

interface IIdField {
  allowNull: boolean; 
  type: DataTypes.StringDataTypeConstructor;
};

interface IstringField {
  allowNull: boolean; 
  type: DataTypes.StringDataTypeConstructor; 
  unique?: boolean;
}

export interface IUserModel { 
  username: IstringField; 
  phone: IstringField;
  birth: DataTypes.DateDataTypeConstructor;
  photo: DataTypes.StringDataTypeConstructor; 
  info: DataTypes.StringDataTypeConstructor; 
  address: DataTypes.StringDataTypeConstructor;
  verify: DataTypes.StringDataTypeConstructor; 
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IPetModel {
    userId: IIdField;
    petname: IstringField,
    view: DataTypes.StringDataTypeConstructor;
    breed: DataTypes.StringDataTypeConstructor;
    sex: DataTypes.StringDataTypeConstructor;
    birth: DataTypes.DateDataTypeConstructor;
    photo: DataTypes.StringDataTypeConstructor;
    character: DataTypes.StringDataTypeConstructor;
    info: DataTypes.StringDataTypeConstructor;
    castrated: DataTypes.AbstractDataTypeConstructor;
    microchipped: DataTypes.AbstractDataTypeConstructor;
    height: DataTypes.FloatDataTypeConstructor
    heightunit: DataTypes.StringDataTypeConstructor;
    mass: DataTypes.FloatDataTypeConstructor
    massunit: DataTypes.StringDataTypeConstructor;
    createdAt: DataTypes.DateDataTypeConstructor;
    updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IStrollModel {
    strollTime: DataTypes.DateDataTypeConstructor;
    latitude: DataTypes.DoubleDataTypeConstructor;
    longitude: DataTypes.DoubleDataTypeConstructor;
    latitudeDelta: DataTypes.DoubleDataTypeConstructor;
    longitudeDelta: DataTypes.DoubleDataTypeConstructor;
    isSingle: DataTypes.AbstractDataTypeConstructor;
    isVisible: DataTypes.AbstractDataTypeConstructor;
    isEnded: DataTypes.AbstractDataTypeConstructor;
    status: DataTypes.StringDataTypeConstructor;
    strollDuration: DataTypes.FloatDataTypeConstructor,
    strollDistance: DataTypes.FloatDataTypeConstructor,
    strollEnergy: DataTypes.FloatDataTypeConstructor,
    finStrollDuration: DataTypes.FloatDataTypeConstructor,
    finStrollDistance: DataTypes.FloatDataTypeConstructor,
    finStrollEnergy: DataTypes.FloatDataTypeConstructor,
    createdAt: DataTypes.DateDataTypeConstructor;
    updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IStrollToPetModel {
  strollId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  petId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IPetFriendsModel {
  petId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  friendId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  query: DataTypes.AbstractDataTypeConstructor;
  appruved: DataTypes.AbstractDataTypeConstructor;
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface ILockModel {
  userId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  lockId: { allowNull: boolean; type: DataTypes.AbstractDataTypeConstructor; };
  createdAt: DataTypes.DateDataTypeConstructor; 
  updatedAt: DataTypes.DateDataTypeConstructor; 
}

export interface IImprovementModel {
    userId: IIdField;
    text: DataTypes.StringDataTypeConstructor;
    status: DataTypes.StringDataTypeConstructor;
    createdAt: DataTypes.DateDataTypeConstructor;
    updatedAt: DataTypes.DateDataTypeConstructor;
}

export interface IBugModel {
    userId: IIdField;
    text: DataTypes.StringDataTypeConstructor;
    status: DataTypes.StringDataTypeConstructor;
    createdAt: DataTypes.DateDataTypeConstructor;
    updatedAt: DataTypes.DateDataTypeConstructor;
}
