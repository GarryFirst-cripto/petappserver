import { LockUserModel } from './models/index';
import BaseRepository from './baseRepository';

class LockRepository extends BaseRepository {
  model: any;

  async create(data: any) {
    const { userId, lockId } = data;
    const createdAt = new Date();
    try {
      const result = await this.model.create({ userId, lockId, createdAt });
      return result;
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async getData(userId:string, lockId:string) {
    return await this.model.findOne({ where: { userId, lockId } })
  }

  async deleteData(userId:string, lockId:string) {
    return await this.model.destroy({ where: { userId, lockId } })
  }

  async deleteLocks(userId:string) {
    const result = await this.model.destroy({ where: { userId }})
    return { result };
  }

}

export default new LockRepository(LockUserModel);