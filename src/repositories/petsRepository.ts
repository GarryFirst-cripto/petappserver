import { PetModel, FriendModel, UserModel, PetFriendsModel } from './models/index';
import BaseRepository from './baseRepository';
import PetFriendRepository from './petFriendRepository';
import StrollMemberRepository from './strollMemberRepository';
import { IListFilter } from '../api/interfaces/interfaces';
import { deleteFile } from '../api/helpers/awsHelper';
import strollMemberRepository from './strollMemberRepository';

class PetRepository extends BaseRepository {
  model: any;

  async getList(filter: IListFilter) {
    const { from: offset, count: limit, ...rest } = filter;
    const where = { ...rest };
    return await this.model.findAll({
      where,
      include: [{
        model: UserModel
      },{
        model: PetFriendsModel,
        attributes: ['id', 'query', 'appruved'],
        include: {
          model: FriendModel,
          include: {
            model: UserModel
          }
        }
      }],      
      offset,
      limit });
  }

  async getByName(petname: string) {
    return await this.model.findOne({ where: { petname } });
  }

  async getById(id: string) {
    return this.model.findOne({
      where: { id },
      include: [{
        model: UserModel
      },{
        model: PetFriendsModel,
        attributes: ['id', 'query', 'appruved'],
        include: {
          model: FriendModel,
          include: {
            model: UserModel
          }
        }
      }]
    });
  }

  async addPetFriend(data: any) {
    const { petId, friendId } = data;
    const friend = await PetFriendRepository.getPetFriend(petId, friendId);
    if (!friend) {
      const appruved = false;
      await PetFriendRepository.create({ petId, friendId, appruved });
    }
    return this.getById(petId);
  }

  async removePetFriend(data: any) {
    const { petId, friendId } = data;
    const result = await PetFriendRepository.deletePetFriend(petId, friendId);
    return { result };
  }

  async deleteUserPets(userId: string) {
    const list = await this.model.findAll({ where: { userId } });
    list.forEach(async (item: any) => {
      await StrollMemberRepository.deletePetStroll(item.id);
      await PetFriendRepository.deleteFriends(item.id);
      await deleteFile(item.photo);
    });
    const result = await this.model.destroy({ where: { userId } });
    return { result };
  }

  async delete(id: string) {
    const pet = await this.getById(id);
    if (pet) {
      await StrollMemberRepository.deletePetStroll(id);
      await PetFriendRepository.deleteFriends(id)
      await deleteFile(pet.photo);
    }
    const result = await this.model.destroy({ where: { id } });
    return { result };
  }

}

export default new PetRepository(PetModel);
