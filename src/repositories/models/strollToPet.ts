import { DataTypes } from 'sequelize'
import { IStrollToPetModel } from '../interfaces/interfaces'

export default (orm: { define: (arg0: string, arg1: IStrollToPetModel, arg2: {}) => any }) => {
  const StrollToPet = orm.define('strolltopet', {
    strollId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    petId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  },{},);

  return StrollToPet
}
