export default (models: { User: any; LockUser: any, Locked: any, Pet: any, PetFriends: any, Friend: any, Stroll: any, Impovement: any, Bug: any }) => {
  const { User, LockUser, Locked, Pet, PetFriends, Friend, Stroll, Impovement, Bug } = models;

  User.hasMany(Pet);
  Pet.belongsTo(User);
  User.hasMany(Friend);
  Friend.belongsTo(User);
  Stroll.belongsToMany(Pet, { through: 'strolltopet' });
  Pet.belongsToMany(Stroll, { through: 'strolltopet' });
  // Pet.belongsToMany(Friend, { through: 'petfriend', foreignKey: 'petId' });
  // Friend.belongsToMany(Pet, { through: 'petfriend', foreignKey: 'friendId' });
  Pet.hasMany(PetFriends, { foreignKey: 'petId' });
  PetFriends.belongsTo(Pet, { foreignKey: 'petId' });
  Friend.hasMany(PetFriends, { foreignKey: 'friendId' });
  PetFriends.belongsTo(Friend, { foreignKey: 'friendId' });
  User.hasMany(Impovement);
  User.hasMany(Bug);
  Impovement.belongsTo(User);
  Bug.belongsTo(User);
  User.hasMany(LockUser, { foreignKey: 'userId' });
  User.hasMany(LockUser, { foreignKey: 'lockId', as: 'blocked' });
  LockUser.belongsTo(User);
  LockUser.belongsTo(Locked, { foreignKey: 'lockId' })
  LockUser.belongsTo(Locked, { foreignKey: 'userId', as: 'blockuser' })
  Locked.hasMany(LockUser, { foreignKey: 'lockId' });
};
