import { DataTypes } from "sequelize";
import { IStrollModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IStrollModel, arg2: {}) => any; }) => {
  const User = sequelize.define('stroll', {
    strollTime: DataTypes.DATE,
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE,
    latitudeDelta: DataTypes.DOUBLE,
    longitudeDelta: DataTypes.DOUBLE,
    isSingle: DataTypes.BOOLEAN,
    isVisible: DataTypes.BOOLEAN,
    isEnded: DataTypes.BOOLEAN,
    status: DataTypes.STRING,
    strollDuration: DataTypes.FLOAT,
    strollDistance: DataTypes.FLOAT,
    strollEnergy: DataTypes.FLOAT,
    finStrollDuration: DataTypes.FLOAT,
    finStrollDistance: DataTypes.FLOAT,
    finStrollEnergy: DataTypes.FLOAT,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
