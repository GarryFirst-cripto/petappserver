import { DataTypes } from "sequelize";
import { IBugModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IBugModel, arg2: {}) => any; }) => {
  const Bug = sequelize.define('bug', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    text: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Bug;
};