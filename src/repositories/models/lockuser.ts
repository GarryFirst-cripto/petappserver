import { DataTypes } from "sequelize";
import { ILockModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: ILockModel, arg2: {}) => any; }) => {
  const LockUser = sequelize.define('lockuser', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    lockId: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return LockUser;
};
