import { DataTypes } from "sequelize";
import { IImprovementModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IImprovementModel, arg2: {}) => any; }) => {
  const Impovement = sequelize.define('impovement', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    text: DataTypes.STRING,
    status: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return Impovement;
};