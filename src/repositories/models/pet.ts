import { DataTypes } from "sequelize";
import { IPetModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IPetModel, arg2: {}) => any; }) => {
  const User = sequelize.define('pet', {
    userId: {
      allowNull: false,
      type: DataTypes.STRING
    },
    petname: {
      allowNull: false,
      type: DataTypes.STRING
    },
    view: DataTypes.STRING,
    breed: DataTypes.STRING,
    sex: DataTypes.STRING,
    birth: DataTypes.DATE,
    photo: DataTypes.STRING,
    character: DataTypes.STRING,
    info: DataTypes.STRING,
    castrated: DataTypes.BOOLEAN,
    microchipped: DataTypes.BOOLEAN,
    height: DataTypes.FLOAT,
    heightunit: DataTypes.STRING,
    mass: DataTypes.FLOAT,
    massunit: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
