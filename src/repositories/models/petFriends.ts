import { DataTypes } from 'sequelize'
import { IPetFriendsModel } from '../interfaces/interfaces'

export default (orm: { define: (arg0: string, arg1: IPetFriendsModel, arg2: {}) => any }) => {
  const PetFriends = orm.define('petfriend', {
    petId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    friendId: {
      allowNull: false,
      type: DataTypes.UUID
    },
    query: DataTypes.BOOLEAN,
    appruved: DataTypes.BOOLEAN,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  },{},);

  return PetFriends
}
