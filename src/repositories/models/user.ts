import { DataTypes } from "sequelize";
import { IUserModel } from "../interfaces/interfaces";

export default (sequelize: { define: (arg0: string, arg1: IUserModel, arg2: {}) => any; }) => {
  const User = sequelize.define('user', {
    username: {
      allowNull: true,
      type: DataTypes.STRING,
      unique: false
    },
    phone: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    birth: DataTypes.DATE,
    photo: DataTypes.STRING,
    info: DataTypes.STRING,
    address: DataTypes.STRING,
    verify: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {});

  return User;
};
