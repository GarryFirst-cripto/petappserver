import sequelize from '../../data/db/connection';
import associate from './associations';
import userModel from './user';
import lockUserModel from './lockuser';
import petModel from './pet';
import strollModel from './stroll';
import strolToPetModel from './strollToPet';
import petFriendsModel from './petFriends';
import impovementModel from './improvement';
import bugModel from './bug';

const User = userModel(sequelize);
const LockUser = lockUserModel(sequelize);
const Locked = userModel(sequelize);
const Pet = petModel(sequelize);
const Friend = petModel(sequelize);
const Stroll = strollModel(sequelize);
const StrollToPet = strolToPetModel(sequelize);
const PetFriends = petFriendsModel(sequelize);
const Impovement = impovementModel(sequelize);
const Bug = bugModel(sequelize);

associate({
  User,
  LockUser,
  Locked,
  Pet,
  PetFriends,
  Friend,
  Stroll,
  Impovement,
  Bug
});

export {
  User as UserModel,
  LockUser as LockUserModel,
  Locked as LockedModel,
  Pet as PetModel,
  Friend as FriendModel,
  Stroll as StrollModel,
  StrollToPet as StrollToPetModel,
  PetFriends as PetFriendsModel,
  Impovement as ImpovementModel,
  Bug as BugModel
};
