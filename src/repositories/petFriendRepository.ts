import { PetFriendsModel } from './models/index';
import BaseRepository from './baseRepository';

class PetFriendRepository extends BaseRepository {
  model: any;

  async create(data: any) {
    const { petId, friendId, appruved } = data;
    const createdAt = new Date();
    try {
      let query = false;
      const result = await this.model.create({ petId, friendId, query, appruved, createdAt });
      query = true;
      await this.model.create({ petId: friendId, friendId: petId, query, appruved, createdAt });
      return result;
    } catch (err) {
      return { status: 400, message: err }
    }
  }

  async getPetFriend(petId:string, friendId:string) {
    return await this.model.findOne({ where: { petId, friendId } })
  }

  async appruveFriend(data: any) {
    const { petId, friendId } = data;
    const query = true;
    const item = await this.model.findOne({ where: { petId, friendId, query } });
    if (item) {
      const { id } = item;
      await this.model.update({ query: false, appruved: true }, {
        where: { id },
        returning: true,
        plain: true
      });
      const subitem = await this.model.findOne({ where: { petId: friendId, friendId: petId } });
      if (subitem) {
        const { id } = subitem;
        await this.model.update({ query: false, appruved: true }, {
          where: { id },
          returning: true,
          plain: true
        });
      }
      return { result: true };
    }
    return { result: false };
  }

  async deletePetFriend(petId:string, friendId:string) {
    await this.model.destroy({ where: { petId: friendId, friendId: petId } })
    return await this.model.destroy({ where: { petId, friendId } })
  }

  async deleteFriends(petId:string) {
    await this.model.destroy({ where: { friendId: petId }})
    return await this.model.destroy({ where: { petId } })
  }

}

export default new PetFriendRepository(PetFriendsModel);