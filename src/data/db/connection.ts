import { Sequelize } from 'sequelize';
import * as config from '../../config/dataConfig';

export default new Sequelize(config);
