const { usersSeed, petsSeed } = require('./seedData/usersSeed');

module.exports = {
  up: async queryInterface => {
    try {
      const data = new Date();

      const usersMappedSeed = usersSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('users', usersMappedSeed, {});

      const petsMappedSeed = petsSeed.map(item => ({
        createdAt: data, ...item
      }));
      await queryInterface.bulkInsert('pets', petsMappedSeed, {});

    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('pets', null, {});
      await queryInterface.bulkDelete('users', null, {});
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(`Seeding error: ${err}`);
    }
  }

};