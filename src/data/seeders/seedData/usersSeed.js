const usersSeed = [
  {
    id: '0461e1a5-df2a-496a-8b01-ba5746ea0c3d',
    username: 'Jhon Garrison',
    phone: '+380663089231',
    info: 'Pet info info info. Pet-User info info info.',
    address: 'User address - address - address'
  },
  {
    id: 'cd4be2b6-9493-4d34-ac7f-da33246dbaaa',
    username: 'Milda Pretty',
    phone: '+380630000000'
  },
  {
    id: 'e15ea52e-748b-4144-abd5-c1b4df83ceec',
    username: 'sysAdmin',
    phone: 'no-admin-phone'
  },
  {
    id: 'b86dff07-0cae-4b5b-8722-f1f5fdb07edd',
    username: 'Tomas Garrison',
    phone: '+380632188031'
  },
  {
    id: 'a52b841e-fd89-46b0-bb5e-069721065f7e',
    username: 'Winona Jhons',
    phone: '+380630002222',
    info: 'Winona Info - info -info - info'
  }
]

const petsSeed = [
  {
    id: '10428f4e-f135-4af8-b1b7-ed2fdc9968a8',
    userId: '0461e1a5-df2a-496a-8b01-ba5746ea0c3d',
    petname: 'Bobik',
    view: 'Dog',
    breed: 'Scary Hound',
    sex: 'male'
  },
  {
    id: 'ab20a23b-f63e-4b15-a303-5fada1a31cd1',
    userId: '0461e1a5-df2a-496a-8b01-ba5746ea0c3d',
    petname: 'Murka',
    view: 'Cat',
    breed: 'Pretty cat',
    sex: 'male'
  },
  {
    id: 'a0278673-0f0f-43a6-9084-2d5b2aa4f905',
    userId: '0461e1a5-df2a-496a-8b01-ba5746ea0c3d',
    petname: 'Mary',
    view: 'Cat',
   breed: 'Pretty cat'
  },
  {
    id: '01ab4eba-2e5c-4b20-a625-6c81ed006e9a',
    userId: 'cd4be2b6-9493-4d34-ac7f-da33246dbaaa',
    petname: 'Mary',
    view: 'Cat',
    breed: 'Pretty cat',
    sex: 'female'
  },
  {
    id: '3a9f76d7-ab80-4f34-8f5d-6d86fb4ca108',
    userId: 'b86dff07-0cae-4b5b-8722-f1f5fdb07edd',
    petname: 'Pirat',
    view: 'Dog',
    breed: 'Strange pirat'
  },
  {
    id: '10d8d4f0-eab6-42e6-b42d-74be14bad89d',
    userId: 'a52b841e-fd89-46b0-bb5e-069721065f7e',
    petname: 'Parrot',
    view: 'Parrot',
    breed: 'Green'
  },
  {
    id: '333e0ffd-ab60-492b-bdca-c07b90188d9e',
    userId: 'a52b841e-fd89-46b0-bb5e-069721065f7e',
    petname: 'Manya',
    view: 'Monkey',
    breed: 'Chimpanzee',
    sex: 'female'
  }
]

module.exports = { usersSeed, petsSeed };
