module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('strolls', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        strollTime: Sequelize.DATE,
        latitude: Sequelize.DOUBLE,
        longitude: Sequelize.DOUBLE,
        latitudeDelta: Sequelize.DOUBLE,
        longitudeDelta: Sequelize.DOUBLE,
        isSingle: Sequelize.BOOLEAN,
        isVisible: Sequelize.BOOLEAN,
        isEnded: Sequelize.BOOLEAN,
        status: Sequelize.STRING,
        strollDuration: Sequelize.FLOAT,
        strollDistance: Sequelize.FLOAT,
        strollEnergy: Sequelize.FLOAT,
        finStrollDuration: Sequelize.FLOAT,
        finStrollDistance: Sequelize.FLOAT,
        finStrollEnergy: Sequelize.FLOAT,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('strolltopets', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        strollId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'strolls',
            key: 'id'
          }
        },
        petId: {
          allowNull: false,
          type: Sequelize.UUID,
          references: {
            model: 'pets',
            key: 'id'
          }
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('strolltopets', { transaction }),
      queryInterface.dropTable('strolls', { transaction })
    ]))
};
