module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'username', { transaction }),
      queryInterface.addColumn('users', 'username', {
        allowNull: true,
        type: Sequelize.STRING,
        unique: false
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
    ]))
};