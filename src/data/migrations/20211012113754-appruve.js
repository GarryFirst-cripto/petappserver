module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.addColumn('petfriends', 'query', {
        type: Sequelize.BOOLEAN
      }, { transaction }),
      queryInterface.addColumn('petfriends', 'appruved', {
        type: Sequelize.BOOLEAN
      }, { transaction })
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('petfriends', 'appruved', { transaction }),
      queryInterface.removeColumn('petfriends', 'query', { transaction })
    ]))
};