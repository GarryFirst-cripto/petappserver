module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .query('CREATE EXTENSION IF NOT EXISTS pgcrypto;')
    .then(() => queryInterface.sequelize.transaction(transaction => Promise.all([
      queryInterface.createTable('users', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        username: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        phone: {
          allowNull: false,
          type: Sequelize.STRING,
          unique: true
        },
        birth: Sequelize.DATE,
        photo: Sequelize.STRING,
        info: Sequelize.STRING,
        address: Sequelize.STRING,
        verify: Sequelize.STRING,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction }),
      queryInterface.createTable('pets', {
        id: {
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('gen_random_uuid()')
        },
        userId: {
          type: Sequelize.UUID,
          references: {
            model: 'users',
            key: 'id'
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL'
        },
        petname: {
          allowNull: false,
          type: Sequelize.STRING
        },
        view: Sequelize.STRING,
        breed: Sequelize.STRING,
        sex: Sequelize.STRING(10),
        birth: Sequelize.DATE,
        photo: Sequelize.STRING,
        character: Sequelize.STRING,
        info: Sequelize.STRING,
        castrated: Sequelize.BOOLEAN,
        microchipped: Sequelize.BOOLEAN,
        height: Sequelize.FLOAT,
        heightunit: Sequelize.STRING(12),
        mass: Sequelize.FLOAT,
        massunit: Sequelize.STRING(12),
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      }, { transaction })      
    ]))),

  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.dropTable('pets', { transaction }),
      queryInterface.dropTable('users', { transaction })
    ]))
};
